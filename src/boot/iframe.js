import { boot } from 'quasar/wrappers';
import { ref } from 'vue';

const ifrdata = ref();

export default boot(
  ({ app }) =>
    new Promise((resolve) => {
      app.provide('iframedata', ifrdata);

      window.addEventListener('message', async (message) => {
        const msgData = message.data;

        ifrdata.value = JSON.stringify(msgData);
        console.log(
          `[IFRAME WINDOW] - [POST MESSAGE DATA] - ${JSON.stringify(msgData)}`
        );
      });
      resolve();
    })
);
