FROM node:16.16.0-alpine as build
WORKDIR /app
COPY package*.json ./
RUN npm install && npm install -g @quasar/cli && npm cache clean --force
COPY . .
RUN quasar build


FROM ubuntu:latest as staging
ENV TZ=Asia/Almaty
ENV NODE_ENV=staging
RUN ln -snf "/usr/share/zoneinfo/$TZ" /etc/localtime
RUN echo "$TZ" > /etc/timezone
RUN apt-get -y update && apt-get -y install nmap apache2 && apt-get clean
COPY apache.conf /etc/apache2/sites-available/apache.conf

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf &&\
    a2enmod rewrite &&\
    a2enmod headers &&\
    a2dissite 000-default &&\
    a2ensite apache &&\
    service apache2 restart

COPY --from=build /app/dist/spa/ /var/www/html

CMD ["sh", "-c", "apache2ctl -D FOREGROUND"]
